import maya.cmds as mc, math

'''
This is a script for determining the next location of the characters main control 
based on given half cycle walk distance.

'''


global charDist112341212
charDist112341212 = {'__user value':('', ''),'char0_Walk': (60, 12), 'char0_Run': (91, 8),
		     'char1_Walk': (57, 11),'char1_Run': (94, 10),
		     'char2_Walk': (50, 10),'char2_Run': (84, 8),
		     'char3_Walk': (50, 10),'char3_Run': (85, 7),
		     'char4_Walk': (55, 11),'char4_Run': (80, 8)}

global fFrootIndex
fFrootIndex = 0

def moveChar(mainCtrl, dist, keys, fFoot ):
    '''
    [str] mainCtrl : name of main control
    [float] dist : distance of half cycle walk
    [float] keys : number of keys of half cycle
    [str] fFoot : front foot, this will be used for feet pivot
    
    '''
    ## querry the current frmae
    currentFrame = mc.currentTime(q = 1)
    ## get the angle 
    angle = mc.getAttr( '%s.ry'%mainCtrl)
    ## go to the current frame to revert back the main control 
    ## rotation to previous rot
    mc.currentTime(currentFrame)
    mc.setKeyframe(mainCtrl, s = 0)
    ## suspend the refresh
    mc.refresh(su = True)
    ## create a locator and parent it to main control [positionLocator]
    ## this will be used for centering the pivot 
    loc = mc.spaceLocator(n = 'asdad')[0]
    mc.delete(mc.parentConstraint( mainCtrl, loc,mo = 0))
    ## querry the pivot location of the front foot 
    pivs = mc.xform(fFoot, q = 1, piv = 1,  ws = 1)[0:3:2]#set pivot on foote
    ## move the pivot of positionLocator to the front foot
    mc.move(pivs[0], 0 , pivs[1],'%s.scalePivot'%loc, '%s.rotatePivot'%loc, rpr = 1,  )
    ## get the current angle and X and Z location of positionLocator 
    x = mc.getAttr('%s.tx'%loc)
    z = mc.getAttr('%s.tz'%loc)

    if angle < 0:
        rot = abs(angle/360)
        angle = (360 - rot) + angle
    ## generate the new location 
    newXZ = getNextPos(x, z, dist, angle)
    ## go to the next keyframe 
    mc.currentTime(currentFrame+keys)
    ## move main control to the next location
    mc.setAttr( '%s.ry'%loc, angle)   
    mc.setAttr('%s.tx'%loc, newXZ[0])
    mc.setAttr('%s.tz'%loc, newXZ[1])
    ## center pivot loc and querry the position
    mc.xform(loc, cp = 1)
    pos = mc.xform(loc, rp = 1, q = 1, ws = 1)
    ## set the main control's new position based on locators
    mc.setAttr( '%s.ry'%mainCtrl, angle)   
    mc.setAttr('%s.tx'%mainCtrl, pos[0])
    mc.setAttr('%s.tz'%mainCtrl, pos[2])
    mc.setKeyframe(mainCtrl, s = 0)
    ## delete locators
    mc.delete(loc)
    ## resume refresh
    mc.refresh(su = False)

def alertMe(msg = 'error'):
    '''
    shows an error dialog box
    [str] msg : text message
    
    '''
    mc.confirmDialog(
	title='Value Error',
	message=msg,
	button=['OK'],
	defaultButton='OK',
	cancelButton='OK',
	dismissString='OK')
    return
 
def makeCharMove(mainCtrl, dist, keys, footCtrl ):
    '''
    moves character based on given input

    [str] mainCtrl : textfield path of main control
    [str] dist : textfield path of distance
    [str] keys : textfield path of number of keys
    [str] footCtrl : textfield path of foot control
    '''
    global frontFootControl100122123e4, fFrootIndex
    
    mainCtrlVal = mc.textField(mainCtrl, q= 1, tx = 1) 
    distVal = mc.textField(dist, q= 1, tx = 1)

    try:          
      keyVal = int(mc.textField(keys, q= 1, tx = 1) )
    except ValueError:
      alertMe('Please input values on\n distance and keys field.')
      return

    moveChar(mainCtrlVal, float(distVal), int(keyVal) , frontFootControl100122123e4[fFrootIndex])
    if fFrootIndex == 0:
      mc.textField(footCtrl, e= 1, tx = '%s  |  %s'%(frontFootControl100122123e4[1], frontFootControl100122123e4[0]))
      fFrootIndex = 1
    else:
      mc.textField(footCtrl, e= 1, tx = '%s  |  %s'%(frontFootControl100122123e4[0], frontFootControl100122123e4[1]))
      fFrootIndex = 0
      
    
    
def getNextPos(x, z, dist = 0, angle = 0):
    '''
    returns the next X and Y pos based on angle, distance and 
    current X Y position

    [float] x : x position of character
    [float] y : y position of character
    [float] dist : distance of one step of character
    [float] angle : facing angle of character

    '''
    
    xpos = dist*math.sin(math.radians(angle))+x
    zpos = dist*math.cos(math.radians(angle))+z

    return xpos, zpos

def usePresets(dist, keys, presetPath  ):
    '''
    updates the UI with preset values for given character
    [str] dist : textfield path of distance value
    [str] keys : textfield path of key value
    [str] presetPath : option menu path of character presets
    
    '''
    val = mc.optionMenu(presetPath,q = 1, v = 1)
    
    dis = charDist112341212[val][0]
    keY = charDist112341212[val][1]
    
    if val == '__user value':
    
        mc.textField(dist, e= 1, ed = 1, tx = '')
        mc.textField(keys, e= 1, ed = 1, tx = '') 
    
    else:
        mc.textField(dist, e= 1, ed = 1, tx = dis)
        mc.textField(keys, e= 1,ed = 1, tx = keY)    



def getMainCtrl(txtfield):
    '''
    gets the control's absolute path from textfield
    [str] txtfield = textfield path of querried controller
    '''
    try:
        command = mc.textField(txtfield, e= 1, tx = mc.ls(sl = 1)[0]) 
    except IndexError:
        print('nothing selected,\n please select main control')
	alertMe('nothing selected,\n please select main control')

def getFootCtrl(txtfield):
    '''
    gets the front control's absolute path from textfield
    [str] txtfield = textfield path of foot control
    '''
    global frontFootControl100122123e4
    
    try:
        command = mc.textField(txtfield, e= 1, tx = '%s  |  %s'%(mc.ls(sl = 1)[0],mc.ls(sl = 1)[1]))
        frontFootControl100122123e4 = [mc.ls(sl = 1)[0], mc.ls(sl = 1)[1]]
    except IndexError:
        print('please select front foot first\n then back foot')
	alertMe('please select front foot first\n then back foot')
	
def walkCtrlUI(selected=''):
  '''
  creates the auto walk UI
  [str] selected : long name of Main control, and foot controls, this can be empty and populated later
  '''
  frame = mc.frameLayout('walkrun UI', l = 'walkrun UI' , lv = 0,bgc = (0.5,.1,.1), font ="fixedWidthFont")
  
  rw = mc.rowColumnLayout( nc=3, p = frame , cw = [(1,80),(2,200),(3,50)])
  mc.text(l = 'main control', p = rw)
  mainCtrl = mc.textField(p = rw)
  mainCtrlPath = mc.textField(mainCtrl, q = 1, fpn = 1)
  mc.button('btn_getMainCtrl', label = 'get', bgc = (.1, .1, .1), c = 'walkChar.getMainCtrl("%s")'%mainCtrlPath, w = 50, h = 30, p = rw)
  mc.text(l = 'front foot ctrl', p = rw)
  fFootCtrl = mc.textField(p = rw)
  fFootCtrlPath = mc.textField(fFootCtrl, q = 1, fpn = 1)
  mc.button('btn_getFfootCtrl', label = 'get', bgc = (.1, .1, .1), c = 'walkChar.getFootCtrl("%s")'%fFootCtrlPath, w = 50, h = 30, p = rw)
  
  if selected != '':
      mc.textField(mainCtrlPath, e = 1, tx = selected[1] )
      mc.textField(fFootCtrlPath, e = 1, tx = selected[0] )
  
  rw1 = mc.rowColumnLayout( nc=6, p = frame , cw = [(1,60),(2,30),(3,50),(4,30),(5,10),(6,150)])
  mc.text(l = 'distance', p = rw1)
  dist = mc.textField(p = rw1, tx = '')
  distPath = mc.textField(dist, q = 1, fpn = 1)

  mc.text(l = 'keys', p = rw1 )
  keys = mc.textField(p = rw1,tx = '')
  keysPath = mc.textField(keys, q = 1, fpn = 1)
  
  mc.text(l = '  ', p = rw1 )
  presetMenu = mc.optionMenu( label='presets', w = 80)
  presetPath = mc.optionMenu(presetMenu, q = 1, fpn = 1)
  mc.optionMenu( presetMenu, e = 1, changeCommand="walkChar.usePresets('%s', '%s' , '%s' )"% (distPath,keysPath, presetPath ))
  
  for i in sorted(charDist112341212):
      mc.menuItem( label=i )
      
  walkRun = 'walkChar.makeCharMove("%s", "%s", "%s", "%s")'%(mainCtrlPath, distPath,  keysPath, fFootCtrlPath)    
  mc.button('btn_moveCharacter', label = '<< MOVE CHARACTER >>', bgc = (.4, .1, .1), c = walkRun, w = 50, h = 30, p = frame)

  mc.setParent('..')
  
def walkCtrlWindow():
  '''
  shows the walk control window  
  '''
  winID = 'walk_run_control'
  
  if mc.window(winID, exists=True):
      mc.deleteUI(winID)

  mc.window(winID, w = 300, h=60, s = 1)
  walkCtrlUI()
  
  mc.showWindow(winID)









walkCtrlWindow()

