'''

This script handles the autowalk of the character. Presets can be made/added thru editing
the charDist112341212 variable. Just chose on the dropdown menu which 
character preset you need. It calculates the next position of the main
control based on the current X and Z position and rotation of the 
character.

You may need to turn off auto key function otherwise this will not work properly
if the character changes direction. 

To use this script

1. copy this in the /home/[user name]/maya/scripts/

2. select the main control of the character you want to walk/run

3. Open a Python console and run:

	  import walkChar
	  reload(walkChar)

    or in MEL console:

	  python("import walkChar;reload(walkChar)");


You may use or not use the 'reload(walkChar)' line, I use it for debugging.


USAGE: 

[Text FIelds]:

>> main control
     name of the main control to be modified
     
>> distance
     distance of a half walk/run cycle
     
>> keys
     number of keyframe of a half walk/run cycle 

[Buttons]:

<< get [main control] >>
    
    select a main control, hit 'get' button, it will update the main control 
    text field. The controller on the textfield will be keyed by the 
    'MOVE CHARACTER' button.

<< get [front foot control] >>
    
    select front foot, shift select the back foot, hit 'get' button,
    This is needed for proper foot pivot when you press 'MOVE CHARACTER' button.
    
<< MOVE CHARACTER >>
    
    go to the frame of a walk/run key pose and hit the button, it will key the 
    controller on the main control textfield


    


Limitations: 

> Errors may arise. This is not a bug free tool. Please do hit me up if issues arise.

To DO:
> Proper documentation of the scripts. I just don't have time. Sorry.


Cheers!

-Robert

'''
 
